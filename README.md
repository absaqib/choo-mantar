# Private Canvas Drawing Tool

Use `grunt watch` to auto compile changes into `widget.min.js`.

```sh
node server.js

# now open:
http://localhost:9001/
```

![image](https://i.imgur.com/UmuWGIq.png)

# It supports

1. Line drawings
2. Pancil tool
3. Circles, rectangles, bezier and quadratice curves, and much more
4. Marker & eraser tools
5. Single or multiple shapes selections
6. Advanced drag/move
7. Coloring, stroke/fill styles, opacities, rounded corners, arrows, line caps and much more
8. Images
9. Advnaced text inputs along with fonts
10. Zooming
11. Scaling
12. Addition features goes here ....