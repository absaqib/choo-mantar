var cropHandler = {
    ismousedown: false,
    prevX: 0,
    prevY: 0,
    mousedown: function(e) {
        var x = e.pageX - canvas.offsetLeft,
            y = e.pageY - canvas.offsetTop;

        var t = this;

        t.prevX = x;
        t.prevY = y;

        t.ismousedown = true;
    },
    mouseup: function(e) {
        var x = e.pageX - canvas.offsetLeft,
            y = e.pageY - canvas.offsetTop;

        var t = this;
        if (t.ismousedown) {
            points[points.length] = ['crop', [0, 0, x, y, t.prevX, t.prevY], drawHelper.getOptions()];

            t.ismousedown = false;
            // setSelection(null, 'DragLastPath');
        }

    },
    mousemove: function(e) {
        var x = e.pageX - canvas.offsetLeft,
            y = e.pageY - canvas.offsetTop;

        var t = this;
        if (t.ismousedown) {
            drawHelper.redraw();
            tempContext.clearRect(0, 0, innerWidth, innerHeight);

            // last parameter "true" is required
            drawHelper.crop(tempContext, [0, 0, x, y, t.prevX, t.prevY, true]);
        }
    }
};
