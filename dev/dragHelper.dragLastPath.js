dragHelper.dragLastPath = function(x, y) {
    var selectedShape = getSelectedShape();
    if (!selectedShape) return;

    if (!dragHelper.isMouseOverSelectedRectangle(x, y, selectedShape)) return

    var prevX = dragHelper.global.prevX,
        prevY = dragHelper.global.prevY,
        p = selectedShape,
        point = p[1],
        getPoint = dragHelper.getPoint,
        getXYWidthHeight = dragHelper.getXYWidthHeight,
        isMoveAllPoints = dragHelper.global.pointsToMove === 'all';

    if (p[0] === 'line') {

        if (dragHelper.global.pointsToMove === 'head' || isMoveAllPoints) {
            point[0] = getPoint(x, prevX, point[0]);
            point[1] = getPoint(y, prevY, point[1]);
        }

        if (dragHelper.global.pointsToMove === 'tail' || isMoveAllPoints) {
            point[2] = getPoint(x, prevX, point[2]);
            point[3] = getPoint(y, prevY, point[3]);
        }

        // selectedShape = [p[0], point, p[2]];
    }

    if (p[0] === 'arrow') {

        if (dragHelper.global.pointsToMove === 'head' || isMoveAllPoints) {
            point[0] = getPoint(x, prevX, point[0]);
            point[1] = getPoint(y, prevY, point[1]);
        }

        if (dragHelper.global.pointsToMove === 'tail' || isMoveAllPoints) {
            point[2] = getPoint(x, prevX, point[2]);
            point[3] = getPoint(y, prevY, point[3]);
        }

        // selectedShape = [p[0], point, p[2]];
    }

    if (p[0] === 'text') {

        if (dragHelper.global.pointsToMove === 'head' || isMoveAllPoints) {
            point[1] = getPoint(x, prevX, point[1]);
            point[2] = getPoint(y, prevY, point[2]);
        }

        // selectedShape = [p[0], point, p[2]];
    }

    if (p[0] === 'arc') {
        if (isMoveAllPoints) {
            point[0] = getPoint(x, prevX, point[0]);
            point[1] = getPoint(y, prevY, point[1]);
        }

        // console.error('pointsToMove', dragHelper.global.pointsToMove);

        if (dragHelper.global.pointsToMove === 'stretch-first') {}

        if (dragHelper.global.pointsToMove === 'stretch-second') {}

        if (dragHelper.global.pointsToMove === 'stretch-third') {}

        if (dragHelper.global.pointsToMove === 'stretch-last') {
            point[2] = getPoint(x, prevX, point[2]);
        }
    }

    if (p[0] === 'rect') {

        if (isMoveAllPoints) {
            point[0] = getPoint(x, prevX, point[0]);
            point[1] = getPoint(y, prevY, point[1]);
        }

        if (dragHelper.global.pointsToMove === 'stretch-first') {
            var newPoints = getXYWidthHeight(x, y, prevX, prevY, {
                x: point[0],
                y: point[1],
                width: point[2],
                height: point[3],
                pointsToMove: dragHelper.global.pointsToMove
            });

            point[0] = newPoints.x;
            point[1] = newPoints.y;
            point[2] = newPoints.width;
            point[3] = newPoints.height;
        }

        if (dragHelper.global.pointsToMove === 'stretch-second') {
            var newPoints = getXYWidthHeight(x, y, prevX, prevY, {
                x: point[0],
                y: point[1],
                width: point[2],
                height: point[3],
                pointsToMove: dragHelper.global.pointsToMove
            });

            point[1] = newPoints.y;
            point[2] = newPoints.width;
            point[3] = newPoints.height;
        }

        if (dragHelper.global.pointsToMove === 'stretch-third') {
            var newPoints = getXYWidthHeight(x, y, prevX, prevY, {
                x: point[0],
                y: point[1],
                width: point[2],
                height: point[3],
                pointsToMove: dragHelper.global.pointsToMove
            });

            point[0] = newPoints.x;
            point[2] = newPoints.width;
            point[3] = newPoints.height;
        }

        if (dragHelper.global.pointsToMove === 'stretch-last') {
            point[2] = getPoint(x, prevX, point[2]);
            point[3] = getPoint(y, prevY, point[3]);
        }

        // selectedShape = [p[0], point, p[2]];
    }

    if (p[0] === 'crop') {

        if (isMoveAllPoints) {
            point[0] = getPoint(x, prevX, point[0]);
            point[1] = getPoint(y, prevY, point[1]);
        }

        if (dragHelper.global.pointsToMove === 'stretch-first') {
            var newPoints = getXYWidthHeight(x, y, prevX, prevY, {
                x: point[0],
                y: point[1],
                width: point[2],
                height: point[3],
                pointsToMove: dragHelper.global.pointsToMove
            });

            point[0] = newPoints.x;
            point[1] = newPoints.y;
            point[2] = newPoints.width;
            point[3] = newPoints.height;
        }

        if (dragHelper.global.pointsToMove === 'stretch-second') {
            var newPoints = getXYWidthHeight(x, y, prevX, prevY, {
                x: point[0],
                y: point[1],
                width: point[2],
                height: point[3],
                pointsToMove: dragHelper.global.pointsToMove
            });

            point[1] = newPoints.y;
            point[2] = newPoints.width;
            point[3] = newPoints.height;
        }

        if (dragHelper.global.pointsToMove === 'stretch-third') {
            var newPoints = getXYWidthHeight(x, y, prevX, prevY, {
                x: point[0],
                y: point[1],
                width: point[2],
                height: point[3],
                pointsToMove: dragHelper.global.pointsToMove
            });

            point[0] = newPoints.x;
            point[2] = newPoints.width;
            point[3] = newPoints.height;
        }

        if (dragHelper.global.pointsToMove === 'stretch-last') {
            point[2] = getPoint(x, prevX, point[2]);
            point[3] = getPoint(y, prevY, point[3]);
        }

        // selectedShape = [p[0], point, p[2]];
    }

    if (p[0] === 'image') {

        if (isMoveAllPoints) {
            point[1] = getPoint(x, prevX, point[1]);
            point[2] = getPoint(y, prevY, point[2]);
        }

        if (dragHelper.global.pointsToMove === 'stretch-first') {
            var newPoints = getXYWidthHeight(x, y, prevX, prevY, {
                x: point[1],
                y: point[2],
                width: point[3],
                height: point[4],
                pointsToMove: dragHelper.global.pointsToMove
            });

            point[1] = newPoints.x;
            point[2] = newPoints.y;
            point[3] = newPoints.width;
            point[4] = newPoints.height;
        }

        if (dragHelper.global.pointsToMove === 'stretch-second') {
            var newPoints = getXYWidthHeight(x, y, prevX, prevY, {
                x: point[1],
                y: point[2],
                width: point[3],
                height: point[4],
                pointsToMove: dragHelper.global.pointsToMove
            });

            point[2] = newPoints.y;
            point[3] = newPoints.width;
            point[4] = newPoints.height;
        }

        if (dragHelper.global.pointsToMove === 'stretch-third') {
            var newPoints = getXYWidthHeight(x, y, prevX, prevY, {
                x: point[1],
                y: point[2],
                width: point[3],
                height: point[4],
                pointsToMove: dragHelper.global.pointsToMove
            });

            point[1] = newPoints.x;
            point[3] = newPoints.width;
            point[4] = newPoints.height;
        }

        if (dragHelper.global.pointsToMove === 'stretch-last') {
            point[3] = getPoint(x, prevX, point[3]);
            point[4] = getPoint(y, prevY, point[4]);
        }

        // selectedShape = [p[0], point, p[2]];
    }

    if (p[0] === 'quadratic') {

        if (dragHelper.global.pointsToMove === 'starting-points' || isMoveAllPoints) {
            point[0] = getPoint(x, prevX, point[0]);
            point[1] = getPoint(y, prevY, point[1]);
        }

        if (dragHelper.global.pointsToMove === 'control-points' || isMoveAllPoints) {
            point[2] = getPoint(x, prevX, point[2]);
            point[3] = getPoint(y, prevY, point[3]);
        }

        if (dragHelper.global.pointsToMove === 'ending-points' || isMoveAllPoints) {
            point[4] = getPoint(x, prevX, point[4]);
            point[5] = getPoint(y, prevY, point[5]);
        }

        // selectedShape = [p[0], point, p[2]];
    }

    if (p[0] === 'bezier') {

        if (dragHelper.global.pointsToMove === 'starting-points' || isMoveAllPoints) {
            point[0] = getPoint(x, prevX, point[0]);
            point[1] = getPoint(y, prevY, point[1]);
        }

        if (dragHelper.global.pointsToMove === '1st-control-points' || isMoveAllPoints) {
            point[2] = getPoint(x, prevX, point[2]);
            point[3] = getPoint(y, prevY, point[3]);
        }

        if (dragHelper.global.pointsToMove === '2nd-control-points' || isMoveAllPoints) {
            point[4] = getPoint(x, prevX, point[4]);
            point[5] = getPoint(y, prevY, point[5]);
        }

        if (dragHelper.global.pointsToMove === 'ending-points' || isMoveAllPoints) {
            point[6] = getPoint(x, prevX, point[6]);
            point[7] = getPoint(y, prevY, point[7]);
        }

        // selectedShape = [p[0], point, p[2]];
    }
};
