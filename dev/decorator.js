var tools = {
    line: true,
    arrow: true,
    pencil: true,
    marker: true,
    arc: true,
    dragSingle: true,
    dragMultiple: true,
    eraser: true,
    rectangle: true,
    arc: true,
    bezier: true,
    quadratic: true,
    text: true,
    image: true,
    zoom: true,
    shapes: true,
    crop: true
};

if (params.tools) {
    try {
        var t = JSON.parse(params.tools);
        tools = t;
    } catch (e) {}
}

function setSelection(element, prop) {
    endLastPath();

    is.set(prop);

    var selected = document.getElementsByClassName('selected-shape')[0];
    if (selected) selected.className = selected.className.replace(/selected-shape/g, '');

    if (!element) {
        element = document.getElementById('drag-last-path');
    }

    if (!element.className) {
        element.className = '';
    }

    element.className += ' selected-shape';
}

/* Default: setting default selected shape!! */
is.set(window.selectedIcon);

window.addEventListener('load', function() {
    var canvasElements = document.getElementsByTagName('img');
    var shape = window.selectedIcon.toLowerCase();

    var firstMatch;
    for (var i = 0; i < canvasElements.length; i++) {
        if (!firstMatch && (canvasElements[i].id || '').indexOf(shape) !== -1) {
            firstMatch = canvasElements[i];
        }
    }
    if (!firstMatch) {
        window.selectedIcon = 'Pencil';
        firstMatch = document.getElementById('pencil-icon');
    }

    setSelection(firstMatch, window.selectedIcon);
}, false);

(function() {
    var cache = {};

    var lineCapSelect = find('lineCap-select');
    var lineJoinSelect = find('lineJoin-select');

    function getContext(id) {
        return find(id);

        var context = find(id).getContext('2d');
        context.lineWidth = 1;
        context.strokeStyle = 'white';
        return context;
    }

    function onExpanderClick(context, ctainer) {
        context.querySelector('.expander-image').onclick = function(e) {
            preventStopEvent(e);

            if (this.src && this.src.indexOf('expander-icon-expanded.png') !== -1) {
                this.src = 'icons/expander-icon.png';
                ctainer.style.display = 'none';
                return;
            }

            fireBodyClick();

            this.src = 'icons/expander-icon-expanded.png';

            var that = this;
            onBodyClickced.push(function() {
                that.src = 'icons/expander-icon.png';
            });

            ctainer.style.top = (context.offsetTop + 1) + 'px';
            ctainer.style.left = (context.offsetLeft + context.clientWidth) + 'px';
            ctainer.style.display = 'block';
        };
    }

    function fireBodyClick() {
        toolTipContainer.style.display = 'none';
        dragResizeContainer.style.display = 'none';
        pencilExpander.style.display = 'none';
        shapeExpander.style.display = 'none';

        onBodyClickced.forEach(function(cb) {
            cb();
        });
        onBodyClickced = [];
    }

    var onBodyClickced = [];
    var toolTipContainer = document.getElementById('tooltip-container');
    var dragResizeContainer = document.getElementById('drag-resize-container');
    var pencilExpander = document.getElementById('pencil-expander');
    var shapeExpander = document.getElementById('shapes-expander');
    document.body.onclick = function() {
        fireBodyClick();
    };

    function handleToolTips(element) {
        if (!element.querySelector('[data-title]')) {
            return;
        }

        var element = element.querySelector('[data-title]');

        var title = element.getAttribute('data-title');
        title && addEvent(element, 'mouseenter', function() {
            if (this.parentNode.className.indexOf('selected-shape') !== -1) return;

            toolTipContainer.style.top = this.parentNode.offsetTop + 'px';
            toolTipContainer.style.left = (this.parentNode.offsetLeft + this.parentNode.clientWidth) + 'px';
            toolTipContainer.innerHTML = title;
            toolTipContainer.style.display = 'block';
        });

        title && addEvent(element, 'mouseleave', function() {
            toolTipContainer.style.display = 'none';
        });
    }

    function bindEvent(context, shape, callback) {
        callback = callback || function() {};

        if (shape === 'Pencil' || shape === 'Marker') {
            lineCap = lineJoin = 'round';
        }

        addEvent(context, 'click', function(e) {
            preventStopEvent(e);
            fireBodyClick();

            callback();

            if (textHandler.text.length) {
                textHandler.appendPoints();
            }

            if (shape === 'Text') {
                textHandler.onShapeSelected();
            } else {
                textHandler.onShapeUnSelected();
            }

            if (shape === 'Pencil' || shape === 'Marker') {
                lineCap = lineJoin = 'round';
            }

            dragHelper.global.startingIndex = 0;

            setSelection(this, shape);

            if (this.id === 'drag-last-path') {
                find('copy-last').checked = true;
                find('copy-all').checked = false;
            } else if (this.id === 'drag-all-paths') {
                find('copy-all').checked = true;
                find('copy-last').checked = false;
            }

            if (this.id === 'image-icon') {
                var selector = new FileSelector();
                selector.selectSingleFile(function(file) {
                    if (!file) return;

                    var reader = new FileReader();
                    reader.onload = function(event) {
                        var image = new Image();
                        image.onload = function() {
                            var index = imageHandler.images.length;

                            imageHandler.lastImageURL = image.src;
                            imageHandler.lastImageIndex = index;

                            imageHandler.images.push(image);
                        };
                        image.src = event.target.result;
                    };
                    reader.readAsDataURL(file);
                });
            }

            if (this.id === 'pencil-icon' || this.id === 'eraser-icon' || this.id === 'marker-icon') {
                cache.lineCap = lineCap;
                cache.lineJoin = lineJoin;

                lineCap = lineJoin = 'round';
            } else if (cache.lineCap && cache.lineJoin) {
                lineCap = cache.lineCap;
                lineJoin = cache.lineJoin;
            }

            if (this.id === 'eraser-icon') {
                cache.strokeStyle = strokeStyle;
                cache.fillStyle = fillStyle;
                cache.lineWidth = lineWidth;

                strokeStyle = 'White';
                fillStyle = 'White';
                lineWidth = 10;
            } else if (cache.strokeStyle && cache.fillStyle && typeof cache.lineWidth !== 'undefined') {
                strokeStyle = cache.strokeStyle;
                fillStyle = cache.fillStyle;
                lineWidth = cache.lineWidth;
            }
        });
    }

    var toolBox = find('tool-box');
    toolBox.style.height = (innerHeight /* - toolBox.offsetTop - 77 */ ) + 'px';

    function decorateDragLastPath() {
        var context = getContext('drag-last-path');
        bindEvent(context, 'DragLastPath');
        onExpanderClick(context, dragResizeContainer);
        handleToolTips(context);
    }

    if (tools.dragSingle === true) {
        decorateDragLastPath();
    } else document.getElementById('drag-last-path').style.display = 'none';

    function decorateDragAllPaths() {
        var context = getContext('drag-all-paths');
        bindEvent(context, 'DragAllPaths');
        handleToolTips(context);
    }

    if (tools.dragMultiple === true) {
        decorateDragAllPaths();
    } else document.getElementById('drag-all-paths').style.display = 'none';

    function decorateShapes() {
        var context = getContext('shapes-icon');

        bindEvent(context, 'Shapes', function() {
            shapeExpander.style.display = 'block';
            shapeExpander.style.top = (context.offsetTop + 1) + 'px';
            shapeExpander.style.left = (context.offsetLeft + context.clientWidth) + 'px';

            shapeExpander.style.display = 'block';
        });
        handleToolTips(context);

    }
    if (tools.shapes === true) {
        decorateShapes();
    } else document.getElementById('shapes-icon').style.display = 'none';


    function decorateLine() {
        var context = getContext('line');
        bindEvent(context, 'Line');
        handleToolTips(context);
    }

    if (tools.line === true) {
        decorateLine();
    } else document.getElementById('line').style.display = 'none';

    function decorateArrow() {
        var context = getContext('arrow');
        bindEvent(context, 'Arrow');
        handleToolTips(context);
    }

    if (tools.arrow === true) {
        decorateArrow();
    } else document.getElementById('arrow').style.display = 'none';

    function decoreZoomUp() {
        var context = getContext('zoom-up');
        bindEvent(context, 'click', function() {
            zoomHandler.up();
        });
        handleToolTips(context);
    }

    function decoreZoomDown() {
        var context = getContext('zoom-down');
        bindEvent(context, 'click', function() {
            zoomHandler.down();
        });
        handleToolTips(context);
    }

    if (tools.zoom === true) {
        decoreZoomUp();
        decoreZoomDown();
    } else {
        document.getElementById('zoom-up').style.display = 'none';
        document.getElementById('zoom-down').style.display = 'none';
    }

    function decoratePencil() {
        var context = getContext('pencil-icon');

        bindEvent(context, 'Pencil');
        onExpanderClick(context, pencilExpander);
        handleToolTips(context);
    }

    if (tools.pencil === true) {
        decoratePencil();
    } else document.getElementById('pencil-icon').style.display = 'none';

    function decorateMarker() {
        var context = getContext('marker-icon');
        bindEvent(context, 'click');
        handleToolTips(context);
    }

    if (tools.marker === true) {
        decorateMarker();
    } else document.getElementById('marker-icon').style.display = 'none';

    function decorateEraser() {
        var context = getContext('eraser-icon');
        bindEvent(context, 'Eraser');
        handleToolTips(context);
    }

    if (tools.eraser === true) {
        decorateEraser();
    } else document.getElementById('eraser-icon').style.display = 'none';

    function decorateText() {
        var context = getContext('text-icon');
        bindEvent(context, 'Text');
        handleToolTips(context);
    }

    if (tools.text === true) {
        decorateText();
    } else document.getElementById('text-icon').style.display = 'none';

    function decorateImage() {
        var context = getContext('image-icon');
        bindEvent(context, 'Image');
        handleToolTips(context);
    }

    if (tools.image === true) {
        decorateImage();
    } else document.getElementById('image-icon').style.display = 'none';

    function decorateArc() {
        var context = getContext('arc');
        bindEvent(context, 'Arc');
        handleToolTips(context);
    }

    if (tools.arc === true) {
        decorateArc();
    } else document.getElementById('arc').style.display = 'none';

    function decorateRect() {
        var context = getContext('rectangle');
        bindEvent(context, 'Rectangle');
        handleToolTips(context);
    }

    if (tools.rectangle === true) {
        decorateRect();
    } else document.getElementById('rectangle').style.display = 'none';

    function decorateCrop() {
        var context = getContext('crop-icon');
        bindEvent(context, 'Crop');
        handleToolTips(context);
    }

    if (tools.rectangle === true) {
        decorateCrop();
    } else document.getElementById('crop-icon').style.display = 'none';

    function decorateQuadratic() {
        var context = getContext('quadratic-curve');
        bindEvent(context, 'QuadraticCurve');
        handleToolTips(context);
    }

    if (tools.quadratic === true) {
        decorateQuadratic();
    } else document.getElementById('quadratic-curve').style.display = 'none';

    function decorateBezier() {
        var context = getContext('bezier-curve');
        bindEvent(context, 'BezierCurve');
        handleToolTips(context);
    }

    if (tools.bezier === true) {
        decorateBezier();
    } else document.getElementById('bezier-curve').style.display = 'none';

    var designPreview = find('design-preview'),
        codePreview = find('code-preview');

    window.selectBtn = function(btn, isSkipWebRTCMessage) {
        codePreview.className = designPreview.className = '';

        if (btn == designPreview) designPreview.className = 'preview-selected';
        else codePreview.className = 'preview-selected';

        if (!isSkipWebRTCMessage && window.connection && connection.numberOfConnectedUsers >= 1) {
            connection.send({
                btnSelected: btn.id
            });
        } else {
            // to sync buttons' UI-states
            if (btn == designPreview) btnDesignerPreviewClicked();
            else btnCodePreviewClicked();
        }
    };

    addEvent(designPreview, 'click', function() {
        selectBtn(designPreview);
        btnDesignerPreviewClicked();
    });

    function btnDesignerPreviewClicked() {
        codeText.parentNode.style.display = 'none';
        optionsContainer.style.display = 'none';

        endLastPath();
    }

    addEvent(codePreview, 'click', function() {
        selectBtn(codePreview);
        btnCodePreviewClicked();
    });

    function btnCodePreviewClicked() {
        codeText.parentNode.style.display = 'block';
        optionsContainer.style.display = 'block';

        codeText.focus();
        common.updateTextArea();

        setHeightForCodeAndOptionsContainer();

        endLastPath();
    }

    var codeText = find('code-text'),
        optionsContainer = find('options-container');

    function setHeightForCodeAndOptionsContainer() {
        codeText.style.width = (innerWidth - optionsContainer.clientWidth - 30) + 'px';
        codeText.style.height = (innerHeight - 40) + 'px';

        codeText.style.marginLeft = (optionsContainer.clientWidth) + 'px';
        optionsContainer.style.height = (innerHeight) + 'px';
    }

    var isAbsolute = find('is-absolute-points'),
        isShorten = find('is-shorten-code');

    addEvent(isShorten, 'change', common.updateTextArea);
    addEvent(isAbsolute, 'change', common.updateTextArea);
})();
