dragHelper.dragAllPaths = function(x, y) {
    var prevX = dragHelper.global.prevX,
        prevY = dragHelper.global.prevY,
        p, point,
        length = points.length,
        getPoint = dragHelper.getPoint,
        i = dragHelper.global.startingIndex;

    for (i; i < length; i++) {
        p = points[i];
        point = p[1];

        if (p[0] === 'line') {
            points[i] = [p[0],
                [
                    getPoint(x, prevX, point[0]),
                    getPoint(y, prevY, point[1]),
                    getPoint(x, prevX, point[2]),
                    getPoint(y, prevY, point[3])
                ], p[2]
            ];
        }

        if (p[0] === 'arrow') {
            points[i] = [p[0],
                [
                    getPoint(x, prevX, point[0]),
                    getPoint(y, prevY, point[1]),
                    getPoint(x, prevX, point[2]),
                    getPoint(y, prevY, point[3])
                ], p[2]
            ];
        }

        if (p[0] === 'text') {
            points[i] = [p[0],
                [
                    point[0],
                    getPoint(x, prevX, point[1]),
                    getPoint(y, prevY, point[2])
                ], p[2]
            ];
        }

        if (p[0] === 'arc') {
            points[i] = [p[0],
                [
                    getPoint(x, prevX, point[0]),
                    getPoint(y, prevY, point[1]),
                    point[2],
                    point[3],
                    point[4]
                ], p[2]
            ];
        }

        if (p[0] === 'rect') {
            points[i] = [p[0],
                [
                    getPoint(x, prevX, point[0]),
                    getPoint(y, prevY, point[1]),
                    point[2],
                    point[3]
                ], p[2]
            ];
        }

        if (p[0] === 'crop') {
            points[i] = [p[0],
                [
                    point[0],
                    point[1],
                    getPoint(x, prevX, point[2]),
                    getPoint(y, prevY, point[3]),
                    getPoint(x, prevX, point[4]),
                    getPoint(y, prevY, point[5])
                ], p[2]
            ];
        }

        if (p[0] === 'image') {
            points[i] = [p[0],
                [
                    point[0],
                    getPoint(x, prevX, point[1]),
                    getPoint(y, prevY, point[2]),
                    point[3],
                    point[4],
                    point[5]
                ], p[2]
            ];
        }

        if (p[0] === 'quadratic') {
            points[i] = [p[0],
                [
                    getPoint(x, prevX, point[0]),
                    getPoint(y, prevY, point[1]),
                    getPoint(x, prevX, point[2]),
                    getPoint(y, prevY, point[3]),
                    getPoint(x, prevX, point[4]),
                    getPoint(y, prevY, point[5])
                ], p[2]
            ];
        }

        if (p[0] === 'bezier') {
            points[i] = [p[0],
                [
                    getPoint(x, prevX, point[0]),
                    getPoint(y, prevY, point[1]),
                    getPoint(x, prevX, point[2]),
                    getPoint(y, prevY, point[3]),
                    getPoint(x, prevX, point[4]),
                    getPoint(y, prevY, point[5]),
                    getPoint(x, prevX, point[6]),
                    getPoint(y, prevY, point[7])
                ], p[2]
            ];
        }
    }
};
