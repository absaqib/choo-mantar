function getSelectedShape() {
    return dragHelper.global.selectedShape;
}

var dragHelper = {
    global: {
        prevX: 0,
        prevY: 0,
        ismousedown: false,
        pointsToMove: 'all',
        startingIndex: 0,
        selectedShape: null
    },
    mousedown: function(e) {
        if (isControlKeyPressed) {
            copy();
            paste();
            isControlKeyPressed = false;
        }

        if (is.isDragAllPaths) {
            dragHelper.mousedownAllPaths(e);
            return;
        }

        var x = e.pageX - canvas.offsetLeft,
            y = e.pageY - canvas.offsetTop;

        dragHelper.detectSelectedShape(e);

        dragHelper.global.prevX = x;
        dragHelper.global.prevY = y;

        dragHelper.global.ismousedown = true;
        dragHelper.setSelectedShapeCircles(e);
    },

    mousedownAllPaths: function(e) {
        var x = e.pageX - canvas.offsetLeft,
            y = e.pageY - canvas.offsetTop;

        dragHelper.global.prevX = x;
        dragHelper.global.prevY = y;

        dragHelper.global.ismousedown = true;
        dragHelper.setSelectedShapeCircles(e);
    },

    setSelectedShapeCircles: function(e) {
        var selectedShape = getSelectedShape();

        if (!selectedShape) return;

        var x = e.pageX - canvas.offsetLeft,
            y = e.pageY - canvas.offsetTop;

        var p = selectedShape,
            point = p[1];

        dragHelper.global.pointsToMove = 'all';

        if (p[0] === 'line') {

            if (dragHelper.isPointInPath(x, y, point[0], point[1])) {
                dragHelper.global.pointsToMove = 'head';
            }

            if (dragHelper.isPointInPath(x, y, point[2], point[3])) {
                dragHelper.global.pointsToMove = 'tail';
            }
        }

        if (p[0] === 'arrow') {

            if (dragHelper.isPointInPath(x, y, point[0], point[1])) {
                dragHelper.global.pointsToMove = 'head';
            }

            if (dragHelper.isPointInPath(x, y, point[2], point[3])) {
                dragHelper.global.pointsToMove = 'tail';
            }
        }

        if (p[0] === 'rect') {

            if (dragHelper.isPointInPath(x, y, point[0], point[1])) {
                dragHelper.global.pointsToMove = 'stretch-first';
            }

            if (dragHelper.isPointInPath(x, y, point[0] + point[2], point[1])) {
                dragHelper.global.pointsToMove = 'stretch-second';
            }

            if (dragHelper.isPointInPath(x, y, point[0], point[1] + point[3])) {
                dragHelper.global.pointsToMove = 'stretch-third';
            }

            if (dragHelper.isPointInPath(x, y, point[0] + point[2], point[1] + point[3])) {
                dragHelper.global.pointsToMove = 'stretch-last';
            }
        }

        if (p[0] === 'arc') {

            if (dragHelper.isPointInPath(x, y, point[0], point[1])) {
                dragHelper.global.pointsToMove = 'stretch-first';
            }

            if (dragHelper.isPointInPath(x, y, point[0] + point[2], point[1])) {
                dragHelper.global.pointsToMove = 'stretch-second';
            }

            if (dragHelper.isPointInPath(x, y, point[0], point[1] + point[3])) {
                dragHelper.global.pointsToMove = 'stretch-third';
            }

            if (dragHelper.isPointInPath(x, y, point[0] + point[2], point[1] + point[3])) {
                dragHelper.global.pointsToMove = 'stretch-last';
            }
        }

        if (p[0] === 'crop') {

            if (dragHelper.isPointInPath(x, y, point[0], point[1])) {
                dragHelper.global.pointsToMove = 'stretch-first';
            }

            if (dragHelper.isPointInPath(x, y, point[0] + point[2], point[1])) {
                dragHelper.global.pointsToMove = 'stretch-second';
            }

            if (dragHelper.isPointInPath(x, y, point[0], point[1] + point[3])) {
                dragHelper.global.pointsToMove = 'stretch-third';
            }

            if (dragHelper.isPointInPath(x, y, point[0] + point[2], point[1] + point[3])) {
                dragHelper.global.pointsToMove = 'stretch-last';
            }
        }

        if (p[0] === 'image') {

            if (dragHelper.isPointInPath(x, y, point[1], point[2])) {
                dragHelper.global.pointsToMove = 'stretch-first';
            }

            if (dragHelper.isPointInPath(x, y, point[1] + point[3], point[2])) {
                dragHelper.global.pointsToMove = 'stretch-second';
            }

            if (dragHelper.isPointInPath(x, y, point[1], point[2] + point[4])) {
                dragHelper.global.pointsToMove = 'stretch-third';
            }

            if (dragHelper.isPointInPath(x, y, point[1] + point[3], point[2] + point[4])) {
                dragHelper.global.pointsToMove = 'stretch-last';
            }
        }

        if (p[0] === 'quadratic') {

            if (dragHelper.isPointInPath(x, y, point[0], point[1])) {
                dragHelper.global.pointsToMove = 'starting-points';
            }

            if (dragHelper.isPointInPath(x, y, point[2], point[3])) {
                dragHelper.global.pointsToMove = 'control-points';
            }

            if (dragHelper.isPointInPath(x, y, point[4], point[5])) {
                dragHelper.global.pointsToMove = 'ending-points';
            }
        }

        if (p[0] === 'bezier') {

            if (dragHelper.isPointInPath(x, y, point[0], point[1])) {
                dragHelper.global.pointsToMove = 'starting-points';
            }

            if (dragHelper.isPointInPath(x, y, point[2], point[3])) {
                dragHelper.global.pointsToMove = '1st-control-points';
            }

            if (dragHelper.isPointInPath(x, y, point[4], point[5])) {
                dragHelper.global.pointsToMove = '2nd-control-points';
            }

            if (dragHelper.isPointInPath(x, y, point[6], point[7])) {
                dragHelper.global.pointsToMove = 'ending-points';
            }
        }
    },
    mouseup: function() {
        if (is.isDragLastPath) {
            // tempContext.clearRect(0, 0, innerWidth, innerHeight);
            context.clearRect(0, 0, innerWidth, innerHeight);
            dragHelper.end();
        }

        dragHelper.global.ismousedown = false;
    },
    detectSelectedShape: function(e) {
        var x = e.pageX - canvas.offsetLeft,
            y = e.pageY - canvas.offsetTop;

        var pointsLength = points.length;
        var ignore = false;
        for (var i = 0; i < pointsLength; i++) {
            if (ignore) return;

            var shapeName = points[i][0];
            if (shapeName === 'rect' || shapeName == 'image') {
                if (dragHelper.isMouseOverSelectedRectangle(x, y, points[i])) {
                    dragHelper.global.selectedShape = points[i];
                    dragHelper.global.selectedShape.position = i;
                    dragHelper.global.prevX = x;
                    dragHelper.global.prevY = y;
                    dragHelper.global.ismousedown = true;
                    ignore = true;
                    continue;
                }
            }

            if (shapeName === 'arc') {
                var point = points[i][1];

                // radius must be greater than or equal to 0
                if (point[2] >= 0) {
                    var path2 = new Path2D();
                    path2.arc(point[0], point[1], point[2], point[3], 0, point[4]);

                    tempContext2.clearRect(0, 0, 9999999, 999999);
                    tempContext2.stroke(path2);

                    if (tempContext2.isPointInPath(path2, x, y)) {
                        dragHelper.global.selectedShape = points[i];
                        dragHelper.global.selectedShape.position = i;
                        dragHelper.global.prevX = x;
                        dragHelper.global.prevY = y;
                        dragHelper.global.ismousedown = true;
                        ignore = true;
                        continue;
                    }
                }
            }
        }
    },
    mousemove: function(e) {
        var x = e.pageX - canvas.offsetLeft,
            y = e.pageY - canvas.offsetTop;

        drawHelper.redraw();

        if (dragHelper.global.ismousedown) {
            dragHelper.dragShape(x, y);
        }

        if (is.isDragLastPath) {
            dragHelper.init(e);
        }
    },
    isMouseOverSelectedRectangle: function(x, y, shape) {
        shape = shape || getSelectedShape();
        if (!shape) return false;

        var shapeName = shape[0];

        if (shapeName !== 'rect' && shapeName !== 'image') return true;

        shape = shape[1];
        if (!shape) return false;

        var shapeX = shape[0];
        var shapeY = shape[1];
        var shapeWidth = shapeX + shape[2];
        var shapeHeight = shapeY + shape[3];

        if (shapeName === 'image') {
            shapeX = shape[1];
            shapeY = shape[2];
            shapeWidth = shapeX + shape[3];
            shapeHeight = shapeY + shape[4];
        }

        if (x > shapeX && x < shapeWidth && y > shapeY && y < shapeHeight) {
            return true;
        }

        return false;
    },
    init: function(e) {
        var x = e.pageX - canvas.offsetLeft,
            y = e.pageY - canvas.offsetTop;

        var selectedShape = getSelectedShape();
        if (!selectedShape) return;

        if (!dragHelper.isMouseOverSelectedRectangle(x, y, selectedShape)) return;

        var p = selectedShape,
            point = p[1];

        if (dragHelper.global.ismousedown) {
            tempContext.fillStyle = 'rgba(255,85 ,154,.9)';
        } else {
            tempContext.fillStyle = 'rgba(255,85 ,154,.4)';
        }

        if (p[0] === 'quadratic') {

            tempContext.beginPath();

            tempContext.arc(point[0], point[1], 10, Math.PI * 2, 0, !1);
            tempContext.arc(point[2], point[3], 10, Math.PI * 2, 0, !1);
            tempContext.arc(point[4], point[5], 10, Math.PI * 2, 0, !1);

            tempContext.fill();
        }

        if (p[0] === 'bezier') {

            tempContext.beginPath();

            tempContext.arc(point[0], point[1], 10, Math.PI * 2, 0, !1);
            tempContext.arc(point[2], point[3], 10, Math.PI * 2, 0, !1);
            tempContext.arc(point[4], point[5], 10, Math.PI * 2, 0, !1);
            tempContext.arc(point[6], point[7], 10, Math.PI * 2, 0, !1);

            tempContext.fill();
        }

        if (p[0] === 'line') {

            tempContext.beginPath();

            tempContext.arc(point[0], point[1], 10, Math.PI * 2, 0, !1);
            tempContext.arc(point[2], point[3], 10, Math.PI * 2, 0, !1);

            tempContext.fill();
        }

        if (p[0] === 'arrow') {

            tempContext.beginPath();

            tempContext.arc(point[0], point[1], 10, Math.PI * 2, 0, !1);
            tempContext.arc(point[2], point[3], 10, Math.PI * 2, 0, !1);

            tempContext.fill();
        }

        if (p[0] === 'text') {
            tempContext.font = "15px Verdana";
            tempContext.fillText(point[0], point[1], point[2]);
        }

        if (p[0] === 'rect') {

            tempContext.beginPath();
            tempContext.arc(point[0], point[1], 10, Math.PI * 2, 0, !1);
            tempContext.fill();

            tempContext.beginPath();
            tempContext.arc(point[0] + point[2] / 2, point[1], 10, Math.PI * 2, 0, !1);
            tempContext.fill();

            tempContext.beginPath();
            tempContext.arc(point[0] + point[2], point[1], 10, Math.PI * 2, 0, !1);
            tempContext.fill();


            tempContext.beginPath();
            tempContext.arc(point[0], point[1] + point[3] / 2, 10, Math.PI * 2, 0, !1);
            tempContext.fill();


            tempContext.beginPath();
            tempContext.arc(point[0], point[1] + point[3], 10, Math.PI * 2, 0, !1);
            tempContext.fill();

            tempContext.beginPath();
            tempContext.arc(point[0] + point[2], point[1] + point[3] / 2, 10, Math.PI * 2, 0, !1);
            tempContext.fill();

            tempContext.beginPath();
            tempContext.arc(point[0] + point[2] / 2, point[1] + point[3], 10, Math.PI * 2, 0, !1);
            tempContext.fill();

            tempContext.beginPath();
            tempContext.arc(point[0] + point[2], point[1] + point[3], 10, Math.PI * 2, 0, !1);
            tempContext.fill();


        }

        if (false && p[0] === 'crop') {
            var x = point[2];
            var y = point[3];
            var prevX = point[4];
            var prevY = point[5];

            tempContext.beginPath();
            tempContext.arc(prevX, prevY, 10, Math.PI * 2, 0, !1);
            tempContext.fill();

            tempContext.beginPath();
            tempContext.arc(x, y, 10, Math.PI * 2, 0, !1);
            tempContext.fill();

            tempContext.beginPath();
            tempContext.arc(prevX, y, 10, Math.PI * 2, 0, !1);
            tempContext.fill();

            tempContext.beginPath();
            tempContext.arc(x, prevY, 10, Math.PI * 2, 0, !1);
            tempContext.fill();
        }

        if (p[0] === 'arc') {
            tempContext.beginPath();
            tempContext.arc(point[0] + point[2], point[1] + point[3], 10, Math.PI * 2, 0, !1);
            tempContext.fill();

            tempContext.beginPath();
            tempContext.arc(point[0] - point[2], point[1], 10, Math.PI * 2, 0, !1);
            tempContext.fill();

            tempContext.beginPath();
            tempContext.arc(point[0], point[1] - point[2], 10, Math.PI * 2, 0, !1);
            tempContext.fill();

            tempContext.beginPath();
            tempContext.arc(point[0], point[1] + point[2], 10, Math.PI * 2, 0, !1);
            tempContext.fill();
        }

        if (p[0] === 'image') {
            tempContext.beginPath();
            tempContext.arc(point[1], point[2], 10, Math.PI * 2, 0, !1);
            tempContext.fill();

            tempContext.beginPath();
            tempContext.arc(point[1] + point[3], point[2], 10, Math.PI * 2, 0, !1);
            tempContext.fill();

            tempContext.beginPath();
            tempContext.arc(point[1], point[2] + point[4], 10, Math.PI * 2, 0, !1);
            tempContext.fill();

            tempContext.beginPath();
            tempContext.arc(point[1] + point[3], point[2] + point[4], 10, Math.PI * 2, 0, !1);
            tempContext.fill();
        }
    },
    isPointInPath: function(x, y, first, second) {
        return x > first - 10 && x < first + 10 && y > second - 10 && y < second + 10;
    },
    getPoint: function(point, prev, otherPoint) {
        if (point > prev) {
            point = otherPoint + (point - prev);
        } else {
            point = otherPoint - (prev - point);
        }

        return point;
    },
    getXYWidthHeight: function(x, y, prevX, prevY, oldPoints) {
        if (oldPoints.pointsToMove == 'stretch-first') {
            if (x > prevX) {
                oldPoints.x = oldPoints.x + (x - prevX);
                oldPoints.width = oldPoints.width - (x - prevX);
            } else {
                oldPoints.x = oldPoints.x - (prevX - x);
                oldPoints.width = oldPoints.width + (prevX - x);
            }

            if (y > prevY) {
                oldPoints.y = oldPoints.y + (y - prevY);
                oldPoints.height = oldPoints.height - (y - prevY);
            } else {
                oldPoints.y = oldPoints.y - (prevY - y);
                oldPoints.height = oldPoints.height + (prevY - y);
            }
        }

        if (oldPoints.pointsToMove == 'stretch-second') {
            if (x > prevX) {
                oldPoints.width = oldPoints.width + (x - prevX);
            } else {
                oldPoints.width = oldPoints.width - (prevX - x);
            }

            if (y < prevY) {
                oldPoints.y = oldPoints.y + (y - prevY);
                oldPoints.height = oldPoints.height - (y - prevY);
            } else {
                oldPoints.y = oldPoints.y - (prevY - y);
                oldPoints.height = oldPoints.height + (prevY - y);
            }
        }

        if (oldPoints.pointsToMove == 'stretch-third') {
            if (x > prevX) {
                oldPoints.x = oldPoints.x + (x - prevX);
                oldPoints.width = oldPoints.width - (x - prevX);
            } else {
                oldPoints.x = oldPoints.x - (prevX - x);
                oldPoints.width = oldPoints.width + (prevX - x);
            }

            if (y < prevY) {
                oldPoints.height = oldPoints.height + (y - prevY);
            } else {
                oldPoints.height = oldPoints.height - (prevY - y);
            }
        }

        return oldPoints;
    },
    dragShape: function(x, y) {
        if (!dragHelper.global.ismousedown) return;

        tempContext.clearRect(0, 0, innerWidth, innerHeight);

        if (is.isDragLastPath) {
            dragHelper.dragLastPath(x, y);
        }

        if (is.isDragAllPaths) {
            dragHelper.dragAllPaths(x, y);
        }

        dragHelper.global.prevX = x;
        dragHelper.global.prevY = y;
    },
    end: function() {
        if (!points.length) return;

        tempContext.clearRect(0, 0, innerWidth, innerHeight);

        var selectedShape = getSelectedShape();
        if (!selectedShape) return;

        var point = selectedShape;
        drawHelper[point[0]](context, point[1], point[2]);
    }
};
