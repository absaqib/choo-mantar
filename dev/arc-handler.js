var arcHandler = {
    ismousedown: false,
    prevX: 0,
    prevY: 0,
    mousedown: function(e) {
        var x = e.pageX - canvas.offsetLeft,
            y = e.pageY - canvas.offsetTop;

        var t = this;

        t.prevX = x;
        t.prevY = y;

        t.ismousedown = true;
    },
    mouseup: function(e) {
        var x = e.pageX - canvas.offsetLeft,
            y = e.pageY - canvas.offsetTop;

        var t = this;
        if (t.ismousedown) {
            var prevX = t.prevX,
                prevY = t.prevY,
                radius = ((x - prevX) + (y - prevY)) / 3;

            points[points.length] = ['arc', [prevX + radius, prevY + radius, radius, Math.PI * 2], drawHelper.getOptions()];

            t.ismousedown = false;
            // setSelection(null, 'DragLastPath');
        }

    },
    mousemove: function(e) {
        var x = e.pageX - canvas.offsetLeft,
            y = e.pageY - canvas.offsetTop;

        var t = this;
        if (t.ismousedown) {
            tempContext.clearRect(0, 0, innerWidth, innerHeight);

            var prevX = t.prevX,
                prevY = t.prevY,
                radius = ((x - prevX) + (y - prevY)) / 3;

            drawHelper.arc(tempContext, [prevX + radius, prevY + radius, radius, Math.PI * 2]);
        }
    }
};
